/**
 * @description       : Getting all the details of given Sobject
 * @author            : mishramayank@concret.in
 * @group             :
 * @last modified on  : 12-04-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log
 * Ver   Date         Author                               Modification
 * 1.0   11-30-2020   mishramayank@concret.in   Initial Version
 **/
public with sharing class GetSObjectDetails{
	//method to get all feilds content dynamically
	@AuraEnabled
	public static List<SObject> getDetail(String name){
		List<SObject> acclist = new List<SObject>();
		List<String> stringfield = getFieldName(name);
		String commasepratedfields = '';
		for (String fieldname : stringfield){
			if (commasepratedfields == null || commasepratedfields == ''){
				commasepratedfields = fieldname;
			} else{
				commasepratedfields = commasepratedfields + ', ' + fieldname;
			}
		}
		string query = 'select Id,' + commasepratedfields + ' from ' + name + ' order by createddate desc';
		try{
			acclist = Database.query(query);
		} catch (Exception e){
			throw new AuraHandledException(e.getMessage());
		}
		return acclist;
	}

	//method to get all the feild  name of specified sObject
	@AuraEnabled
	public static List<String> getFieldName(String name){
		Map<String, Schema.SObjectField> fieldmap = getFeildMap(name);
		Map<String, Schema.DescribeFieldResult> feildinfo = getFeildInfo(fieldmap);

		// System.debug(keymap);
		List<String> keyset = new List<String>(feildinfo.keySet());
		return keyset;
	}

	//method to return Map<String, Schema.SObjectField> from Object Api Name
	//get all the feilds of given sObject
	public static Map<String, Schema.SObjectField> getFeildMap(String name){
		Schema.SObjectType s = Schema.getGlobalDescribe().get(name);  //getGlobalDescribe return  map of all Sobjects names( Keys) to sObject tokens(api name) (values)
		Schema.DescribeSObjectResult r = s.getDescribe();
		Map<String, Schema.SObjectField> fieldmap = r.fields.getMap();  //return map having feild name and feild properties
		//system.debug(fieldmap);
		return fieldmap;
	}

	//return map of feild API name and its all information (like isUpdateable,isCreateable,etc)
	public static Map<String, Schema.DescribeFieldResult> getFeildInfo(Map<String, Schema.SObjectField> fieldmap){
		Map<String, Schema.DescribeFieldResult> keymap = new Map<String, Schema.DescribeFieldResult>();
		for (String key : fieldmap.keyset()){
			Schema.SObjectField field = fieldmap.get(key);
			Schema.DescribeFieldResult F = field.getDescribe();
			if (F.isUpdateable() == true || F.isCreateable() == true || F.getLocalName() == 'Name'){
				keymap.put(F.getLocalName(), F);
			}
		}
		return keymap;
	}

	//method to delete the list of Id with specific sObject.
	@AuraEnabled
	public static void deleteDetail(List<Id> Id, String name){
		List<SObject> lid = new List<SObject>();
		for (Object eleId : Id){
			SObject sObj = Schema.getGlobalDescribe().get(name).newSObject();
			sObj.Id = (Id)eleId;
			lid.add(sObj);
		}

		try{
			delete lid;
		} catch (DmlException e){
			throw new AuraHandledException(e.getMessage());
		}
	}

	//method to insert data of specific sObject
	@AuraEnabled
	public static void insertData(Map<String, String> data, string name){

		SObject sdata = Schema.getGlobalDescribe().get(name).newSObject();
		Map<String, Schema.SObjectField> fieldmap = getFeildMap(name);
		Map<String, Schema.DescribeFieldResult> feildinfo = getFeildInfo(fieldmap);
		for (String key : data.keySet()){
			Schema.DescribeFieldResult F = feildinfo.get(key);
			String fieldstringvalue = data.get(key);
			object fieldvalue = null;
			//to populate only that feild value which had been entered by user
			if (fieldstringvalue != null){
				//Dynamically Convert String result to its specified type
				if (F.getSOAPType() == Schema.SoapType.Date)
					fieldvalue = Date.valueOf(fieldstringvalue);
				else if (F.getSOAPType() == Schema.SoapType.DateTime)
					fieldvalue = DateTime.valueOf(fieldstringvalue);
				else if (F.getSOAPType() == Schema.SoapType.Boolean)
					fieldvalue = Boolean.valueOf(fieldstringvalue);
				else if (F.getSOAPType() == Schema.SoapType.Id)
					fieldvalue = Id.valueOf(fieldstringvalue);
				else if (F.getSOAPType() == Schema.SoapType.Double)
					fieldvalue = Double.valueOf(fieldstringvalue);
				else if (F.getSOAPType() == Schema.SoapType.Integer)
					fieldvalue = Integer.valueOf(fieldstringvalue);
				else if (F.getSOAPType() == Schema.SoapType.String)
					fieldvalue = fieldstringvalue;
				sdata.put(key, fieldvalue);
			}
		}
		try{
			insert sdata;
		} catch (Exception e){
			throw new AuraHandledException(e.getMessage());
		}
	}
}