import { api, LightningElement, track } from 'lwc';
import getFieldName from '@salesforce/apex/GetSObjectDetails.getFieldName';
import insertData from '@salesforce/apex/GetSObjectDetails.insertData';


export default class CreateSobjectModal extends LightningElement {
    @api objectApiName;         //object API name
    isnodata = true;            //boolean to run spinner
    @track feildsname = [];       //Feild name that are comming from server to insert record

    //method to get all the feilds Api name from controller
    connectedCallback() {
        getFieldName({
            name: this.objectApiName
        }).then((result) => {
            this.feildsname = result;
            if (this.feildsname.includes('LastName')) { //removing Name attribute if it has LastName and FirstName attribute to reduce redundency 
                var removeIndex = this.feildsname.map((item) => { return item }).indexOf("Name");
                this.feildsname.splice(removeIndex, 1);
            }
            this.isnodata = false;
        }).catch((error) => {
            console.log(error);
        })
    }

    //method to sumbit response to server
    handleSubmit(event) {
        event.preventDefault();       // stop the form from submitting
        const fields = event.detail.fields;
        insertData({
            data: fields,
            name: this.objectApiName
        }).then(() => {
            this.handleModalSumbit();
        }).catch((errors) => {
            console.log(errors);
        })
    }

    //send message to parent dataManager component that records
    handleModalSumbit() {
        const sumbitmodal = new CustomEvent('success');
        this.dispatchEvent(sumbitmodal);

    }


    //request dataManager to close Modal
    closeCreateModal() {

        const closemodal = new CustomEvent('close');
        this.dispatchEvent(closemodal);

    }
}