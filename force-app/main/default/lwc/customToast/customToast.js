import { LightningElement, api, track } from 'lwc';

export default class CustomToast extends LightningElement {

    @track type;                        //Type of Toast message to show (error,success,warning)
    @track message;                     //message to show on toast
    @track showToastBar = false;        //weather to show toastbar or not
    autoCloseTime = 4000;

    //method to show toast 
    @api
    showToast(type, message) {
        this.type = type;
        this.message = message;
        this.showToastBar = true;
        setTimeout(() => {
            this.closeToast();
        }, this.autoCloseTime);
    }

    //method to close toast
    closeToast() {
        this.showToastBar = false;
        this.type = '';
        this.message = '';
    }

    //getter to get Icon
    get getIconName() {
        return 'utility:' + this.type;
    }

    //getter to get inner class
    get innerClass() {
        return 'slds-icon_container slds-icon-utility-' + this.type + ' slds-icon-utility-success slds-m-right_small slds-no-flex slds-align-top';
    }

    //getter to get outer class
    get outerClass() {
        return 'slds-notify slds-notify_toast slds-theme_' + this.type;
    }
}