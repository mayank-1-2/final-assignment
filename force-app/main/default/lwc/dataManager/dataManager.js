import { LightningElement, api, track } from 'lwc';
import getDetail from '@salesforce/apex/GetSObjectDetails.getDetail';
import deleteDetail from '@salesforce/apex/GetSObjectDetails.deleteDetail';


export default class DataManager extends LightningElement {
    message;                        //message for toast
    accountList = [];               //initially tabledata is stored here
    error;                          //error that came from server side 
    cols;                           //getting dynamic columns
    columns;                        //removed Id and Name and added url for redirection in cols         
    recordlen;                      //length of record came from server side
    @track value = [];              //value for page size in combo-box
    selectedrows = [];                //Info of rows that are selected to delete
    selectedrowslen = 0;              //length of selected rows
    pageNumber = 1;                 //Page Number (Pagination)
    @track searchKey = 'Account'    //Getting sObject dynamically
    @track recordsperpage = 10;     //No. of records to show in one page
    @track recordstodisplay = [];   //Info of records to dispaly in one page               
    isdelete = true;                  //boolean to disable delete button
    @track pages = [];              //List that contain page number in sequence
    chunks = 1;                     //chunk or collection of pages to show at once
    @track pagestodisplay = [];     // Pages to show on current chunk
    @api isModalOpen = false;         //boolean to open or close modal
    isdisablednext = false;         //boolean to disable next button
    isdisabledprev = true;          //boolean to disable prev. button
    isnodata = true;                  //boolean to run spinner


    //Initially getting all table data
    constructor() {
        super();
        this.getTableData();
    }

    //options for lightning-combobox
    get options() {
        return [
            { label: '10', value: 10 },
            { label: '15', value: 15 },
            { label: '20', value: 20 },
        ];
    }

    //this hook is to highlight page button  
    renderedCallback() {
        this.renderButtons();
    }

    //added styling to index button 
    renderButtons = () => {
        this.template.querySelectorAll('.pagination').forEach((but) => {
            but.style.backgroundColor = this.pageNumber === parseInt(but.dataset.id, 10) ? 'blue' : 'white';
            but.style.color = this.pageNumber === parseInt(but.dataset.id, 10) ? 'white' : 'blue';
            but.style.borderRadius = '20px';
            but.style.cursor = 'pointer';
        });
    }

    //called apex controller to fetch records of Specified sObject object.
    getTableData() {
        this.isnodata = true;
        getDetail({
            name: this.searchKey
        }).then(result => {
            this.accountList = result;
            this.recordlen = this.accountList.length;
            this.reRenderPageNo();
            this.setRecordsToDisplay();
            this.makeCols(this.accountList);
            this.error = undefined;
            this.isnodata = false;

        }).catch((error) => {
            this.showToast('error', error.body.message);
            this.error = error;
            this.accountList = [];
        })
    }

    //method to reload PageNumber
    reRenderPageNo() {
        this.pageNumber = 1;
        this.pages = [];
        this.chunks = 0;
        this.pagestodisplay = [];
        this.addTotalNumberOfPages();
        this.pagestodisplay = this.pages.slice(0, 3);
        this.isdisablednext = false;
    }

    //method to find out total number of pages to be formed
    addTotalNumberOfPages() {
        let numberOfPages = Math.ceil(this.recordlen / this.recordsperpage);
        for (let index = 1; index <= numberOfPages; index++) {
            this.pages.push(index);
        }
    }



    //Dynamically making columns
    makeCols(data) {
        this.cols = Object.keys(data[0])
            .reduce((cols, key) => {

                return cols.concat({
                    label: key,
                    fieldName: key,
                    type: 'text',
                });

            }, []);
        this.cols.length = 6;
        this.columns = this.cols;
        this.columns.unshift({
            label: `${this.searchKey} Name`, fieldName: 'recordLink', type: 'url', sortable: true,
            typeAttributes: { label: { fieldName: 'Name' }, target: '_blank' }
        })
        var removeIndex = this.columns.map((item) => { return item.fieldName; }).indexOf("Name"); //removing Name feild because Link has been added
        this.columns.splice(removeIndex, 1);
        var removeIndex = this.columns.map((item) => { return item.fieldName; }).indexOf("Id");  // removing Id Feild from Columns
        this.columns.splice(removeIndex, 1);

    }

    //methos to get the type of sObject
    handleSobjectChange(event) {
        this.searchKey = event.target.value;
        this.getTableData();
    }

    //method to get succes response from Modal component and send messame to Toast component
    sobjectAdded() {
        this.showHideSobjectModal();
        this.getTableData();
        this.showToast('success', this.searchKey + ' added');
    }

    //method to request to show toast
    showToast(status, message) {
        this.template.querySelector('c-custom-toast').showToast(status, message);
    }

    //method to toggle modal
    showHideSobjectModal() {
        this.isModalOpen = !(this.isModalOpen);
    }


    //selected rows for any operations from table
    getSelectedName(event) {
        this.selectedrows = event.detail.selectedRows;

        // Display that fieldName of the selected rows
        this.selectedrowslen = this.selectedrows.length;
        if (this.selectedrowslen === 0) {
            this.isdelete = true;
        } else {
            this.isdelete = false;
        }

    }


    //method to delete sepcified data
    handleDelete() {
        var Id = [];
        this.selectedrows.forEach((element) => {
            Id.push(element.Id);   //sending the list of Id's to delete at backend.
        })
        deleteDetail({
            Id: Id,
            name: this.searchKey
        }).then(() => {
            this.showToast('success', this.selectedrowslen + ' ' + this.searchKey + '\' s data has been' + '  deleted');
            this.selectedrowslen = 0;
            this.pageNumber = 1;
            this.getTableData(); //Rerendering the DataTable when new record is created or edited.   
        }).catch((errors) => {
            this.showToast('error', this.searchKey + 's ' + errors);
        })

    }



    //function to show next chunk of page number
    increaseChunk() {
        this.chunks += 1;
        this.pagestodisplay = this.pages.slice((this.chunks - 1) * 3, (this.chunks) * 3);
        if (this.chunks >= Math.floor(this.pages.length / 3)) {
            this.isdisablednext = true;
        }
        else {
            this.isdisablednext = false;
        }
        this.renderButtons();


    }

    //function to show previous chunk of page number
    decreaseChunk() {

        if (this.chunks > 1) {
            this.chunks -= 1;
            this.pagestodisplay = this.pages.slice((this.chunks - 1) * 3, (this.chunks) * 3);
        }
        this.renderButtons();
    }


    //funtion to control next button in Page
    nextPage() {
        this.pageNumber = this.pageNumber + 1;
        if (this.pageNumber > this.chunks * 3) {
            this.increaseChunk();
        }
        this.setRecordsToDisplay();

    }


    //funtion to control previous button in Page
    previousPage() {
        this.pageNumber = this.pageNumber - 1;
        if (this.pageNumber < (this.chunks * 3) - 2) {
            this.decreaseChunk();
        }
        this.setRecordsToDisplay();
    }


    //function to handle the change in combobox
    handleChange(event) {
        this.chunks = 1;
        this.recordsperpage = event.target.value;  //change the page layout size
        this.reRenderPageNo();
        this.setRecordsToDisplay();
    }

    //funtion to see which pagenumber has been clicked
    onPageClick = (e) => {
        this.pageNumber = parseInt(e.target.dataset.id, 10);
        this.setRecordsToDisplay();
    }


    //method to check weather to disable or not next or prev button
    checkButtons() {
        if (this.pageNumber === this.pages[(this.pages.length) - 1]) {
            this.isdisablednext = true;
        } else {
            this.isdisablednext = false;
        }
        if (this.pageNumber === 1) {
            this.isdisabledprev = true;
        } else {
            this.isdisabledprev = false;
        }


    }

    //method to show record on specified page number
    setRecordsToDisplay() {
        this.checkButtons();
        this.recordstodisplay = [];
        // for last few elements which is less than the page size
        if ((this.pageNumber * this.recordsperpage) > this.accountList.length - (this.accountList.length) % this.recordsperpage) {
            for (let i = (this.pageNumber - 1) * this.recordsperpage; i < ((this.pageNumber - 1) * this.recordsperpage) + ((this.accountList.length) % this.recordsperpage); i++) {
                let tempRecord = Object.assign({}, this.accountList[i]); //cloning object  
                tempRecord.recordLink = "/" + tempRecord.Id; //adding link to redirect to records page
                this.recordstodisplay.push(tempRecord);
            }
        }
        else {
            for (let i = (this.pageNumber - 1) * this.recordsperpage; i < this.pageNumber * this.recordsperpage; i++) {
                let tempRecord = Object.assign({}, this.accountList[i]); //cloning object  
                tempRecord.recordLink = "/" + tempRecord.Id;  //adding link to redirect to records page
                this.recordstodisplay.push(tempRecord);
            }

        }
    }


}